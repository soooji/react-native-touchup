import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import HeadBar from '../../Base/HeadBar'
import Icon from 'react-native-vector-icons/Feather'
import { COLORS, Normalize, WIN, FONTS } from '../../Base/functions'
import { TextInput } from 'react-native-gesture-handler'
export default class SearchScreen extends Component {
  render () {
    return (
      <View>
        <HeadBar isSearchScreen={true} center={<TextInput
                                                 placeholder="جستجو"
                                                 style={styles.searchInput}
                                                 clearButtonMode="while-editing"
                                                 autoCapitalize="none"
                                                 autoCorrect={false} />} />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  searchInput: {
    backgroundColor: COLORS.LighterGrey,
    width: WIN.width - 30,
    borderRadius: 8,
    height: 35,
    color: COLORS.DarkGrey,
    textAlign: 'center',
    fontFamily: FONTS.medium,
    fontSize: Normalize(13),
    marginLeft: 'auto',
    direction: 'rtl',
    marginRight: 'auto'
  }
})

import React, { Component } from 'react'
import { Text, View, TouchableOpacity} from 'react-native'
import HeadBar from '../../Base/HeadBar'
import Icon from 'react-native-vector-icons/Feather'
import Icon2 from 'react-native-vector-icons/FontAwesome5'
import { COLORS, Normalize, WIN, FONTS, SIZES } from '../../Base/functions'
export default class PostScreen extends Component {
  render () {
    return (
      <View>
        <HeadBar right={<TouchableOpacity style={{paddingHorizontal: 10}}>
                         <Icon2 name={"tshirt"} color={COLORS.DarkGrey} size={Normalize(18)} />
                       </TouchableOpacity>} left={<TouchableOpacity style={{paddingHorizontal: 10}}>
                         <Icon name={"info"} color={COLORS.DarkGrey} size={Normalize(18)} />
                       </TouchableOpacity>} center={<View>
                         <Text style={{fontFamily:FONTS.demibold,fontSize:Normalize(SIZES.pageTitle)}}>طرح جدید</Text>
                       </View>} />
      </View>
    )
  }
}

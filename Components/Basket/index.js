import React, { Component } from 'react'
import { Text, View, TouchableOpacity} from 'react-native'
import HeadBar from '../../Base/HeadBar'
import Icon from 'react-native-vector-icons/Feather'
import { COLORS, Normalize, WIN, FONTS,SIZES } from '../../Base/functions'
export default class BasketScreen extends Component {
  render () {
    return (
      <View>
        <HeadBar right={<TouchableOpacity style={{paddingHorizontal:10}}>
                         <Icon name={"info"} color={COLORS.DarkGrey} size={Normalize(18)} />
                       </TouchableOpacity>} center={<View><Text style={{fontFamily:FONTS.demibold,fontSize:Normalize(SIZES.pageTitle)}}>سبد خرید</Text></View>} />
      </View>
    )
  }
}

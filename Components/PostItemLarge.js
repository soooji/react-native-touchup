import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet,Image, FlatList } from 'react-native'
import { COLORS, Normalize, WIN, FONTS, SIZES, FLEX } from '../Base/functions'
import Icon from 'react-native-vector-icons/Feather'
import Icon2 from 'react-native-vector-icons/FontAwesome5'
import ImageProgress from 'react-native-image-progress';
import { ScrollView } from 'react-native-gesture-handler';
// import Progress from 'react-native-progress';

export default class PostItemLarge extends Component {
  constructor(props) {
    super(props);
    this.state =  {
      viewSize: 0,
      height: 0
    }
  }
  render () {
    return (
      <View style={styles.container} onLayout={(e)=>{this.setState({viewSize: e.nativeEvent.layout.width})}}>
        <View style={[styles.titleBox]}>
          <View style={FLEX.RNwSbCC}>
            <View style={FLEX.RNwFsCS}>
              <Image source={{uri: `${this.props.user_avatar}`}} style={styles.avatar}/>
              <Text style={styles.userName}>sajadbeheshti</Text>
            </View>
            <View style={[FLEX.RNwFeCS]}>
              <TouchableOpacity style={{paddingVertical:5,paddingHorizontal:10}} activeOpacity={.7}>
                <Icon2 name="bookmark" solid={this.props.isBookmarked} color={COLORS.DarkGrey} size={Normalize(15)}/>
              </TouchableOpacity>
              <TouchableOpacity style={{paddingVertical:5,paddingHorizontal:10}} activeOpacity={.7}>
                <Icon2 name="heart" solid={this.props.isLiked} color={this.props.isLiked ? COLORS.MainColor : COLORS.DarkGrey} size={Normalize(15)}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <TouchableOpacity>
          <ImageProgress
            source={{uri: `${this.props.product_image_url}`}}
            style={{width:this.state.viewSize,height:this.state.viewSize}}
          />
        </TouchableOpacity>
        <View style={{width:'100%',height:1,backgroundColor:COLORS.LighterGrey}}></View>
        <View>
          <Text style={styles.postTitle} numberOfLines={1}>
            {this.props.title}
          </Text>
          <FlatList
            horizontal={true}
            style={styles.tagList}
            contentContainerStyle={{paddingLeft:10}}
            inverted={true}
            showsHorizontalScrollIndicator={false}
            data={this.props.tags}
            renderItem={(v)=><TouchableOpacity activeOpacity={.7} style={styles.tagItem}><Text style={styles.tagText}>{v.item.title}</Text></TouchableOpacity>}
          />
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  tagList : {
    width:'100%',
    paddingBottom: 10
  },
  tagText : {
    fontFamily: FONTS.light,
    fontSize: Normalize(10),
    color:'black',
  },
  tagItem : {
    backgroundColor:'rgba(0,0,0,.02)',
    borderRadius:7,
    paddingVertical:4,
    paddingHorizontal:8,
    borderWidth:1,
    borderColor: COLORS.LighterGrey,
    marginLeft:10
  },
  postTitle : {
    fontFamily: FONTS.demibold,
    fontSize: Normalize(11),
    textAlign:'right',
    color:'black',
    width: '100%',
    padding: 15
  },
  userName : {
    fontFamily: FONTS.regular,
    letterSpacing:.5,
    fontSize: Normalize(12.5),
    textAlign:'left',
    paddingLeft:9,
    paddingTop:5,
    color:COLORS.DarkGrey
  },
  avatar : {
    width: 32,
    height: 32,
    borderRadius: 32/2
  },
  titleBox : {
    width: '100%',
    borderBottomColor: COLORS.LighterGrey,
    borderBottomWidth: 1,
    padding: 12,
  },
  container: {
    width: '100%',
    marginBottom: 20,
    borderRadius: 15,
    backgroundColor: 'white',
    height: 'auto',
    // paddingBottom:20,
    shadowColor: 'rgba(0,0,0,.12)',
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowRadius : 10,
    shadowOpacity: 1
  }
})

// componentWillMount() {
  //   Image.getSize(this.props.product_image_url, (width, height) => {
  //       this.setState({ height: height * this.state.viewSize / width });
  //   });
  // }
  // http://sajadbeheshti.ir/Untitled.hyperesources/avatar_2x.jpg
  // https://snaptee.co/model/atfck/1
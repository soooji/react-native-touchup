import React, { Component } from 'react'
import { Text, View, TouchableOpacity} from 'react-native'
import HeadBar from '../../Base/HeadBar'
import Icon from 'react-native-vector-icons/Feather'
import { COLORS, Normalize, WIN,FONTS,SIZES } from '../../Base/functions'
export default class ProfileScreen extends Component {
  render () {
    return (
      <View>
        <HeadBar left={<TouchableOpacity style={{paddingHorizontal: 10}}>
                         <Icon name={"rss"} color={COLORS.DarkGrey} size={Normalize(18)} />
                       </TouchableOpacity>} right={<TouchableOpacity style={{paddingHorizontal: 10}}>
                         <Icon name={"settings"} color={COLORS.DarkGrey} size={Normalize(18)} />
                       </TouchableOpacity>} center={<View>
                         <Text style={{fontFamily:FONTS.demibold,fontSize:Normalize(SIZES.pageTitle)}}>صفحه من</Text>
                       </View>}/>
      </View>
    )
  }
}

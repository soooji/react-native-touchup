import React, { Component } from 'react'
import { Text, View, TouchableOpacity, RefreshControl } from 'react-native'
import HeadBar from '../../Base/HeadBar'
import Icon from 'react-native-vector-icons/Feather'
import AutoHeightImage from 'react-native-auto-height-image'
import { COLORS, Normalize, WIN, SIZES } from '../../Base/functions'
import { ScrollView } from 'react-navigation'
import PostItemLarge from '../PostItemLarge'
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    }
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    setTimeout(()=>this.setState({refreshing:false}),3000)
  }
  
  render () {
    return (
      <View>
        <HeadBar left={<TouchableOpacity style={{paddingHorizontal: 10}}>
                         <Icon name={"rss"} color={COLORS.DarkGrey} size={Normalize(18)} />
                       </TouchableOpacity>} right={<Text></Text>} center={<AutoHeightImage source={require('./../../Static/logo-red.png')} width={WIN.width / 4.3} />} />
        <ScrollView 
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        style={{width: '100%',marginBottom: SIZES.headBarHeight}} contentContainerStyle={{paddingHorizontal: 15,paddingTop: 15}}>
          <PostItemLarge user_avatar='http://sajadbeheshti.ir/Untitled.hyperesources/avatar_2x.jpg' tags={[{id:1,title:'تی‌شرت'},{id:2,title:'مردانه'},{id:3,title:'دریا'},{id:3,title:'تیره'},{id:4,title:'خاک'},{id:5,title:'ایران'},{id:6,title:'وطن'},{id:2,title:'مردانه'},{id:3,title:'دریا'}]} title={"تی‌شرت طرح دریا در قاب با طرح های زیبا، جادار و مطمئن بدون درد و خونریزی"} product_image_url='https://snaptee.co/model/atfck/1' isBookmarked={false} isLiked={false}/>
          <PostItemLarge user_avatar='http://sajadbeheshti.ir/Untitled.hyperesources/avatar_2x.jpg' tags={[{id:1,title:'هدفون'},{id:2,title:'موسیقی'},{id:3,title:'موزیک'},{id:3,title:'سفید'},{id:4,title:'پاپ'},{id:5,title:'هودی'},{id:6,title:'مردانه'},{id:2,title:'سوییشرت'},{id:3,title:'تمیز'}]} title={"تی‌شرت طرح دریا در قاب"} product_image_url='https://d1tb5im0xynh1.cloudfront.net/media/tee_designs/2017/09/me47k/model-4.jpg?updated=1509969739' isBookmarked={true} isLiked={false} />
        </ScrollView>
      </View>
    )
  }
}

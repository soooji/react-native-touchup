package com.touchup;

import com.facebook.react.ReactActivity;

import com.facebook.react.ReactActivityDelegate; //i've added
import com.facebook.react.ReactRootView; // i've added
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView; // i've added
public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Touchup";
    }
    //i've addde below
    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
                protected ReactRootView createRootView() {
                    return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }
}

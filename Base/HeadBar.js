import React, { Component } from 'react'
import { Text, View,StyleSheet,Image } from 'react-native'
import { WIN, COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX, SIZES } from './functions';
import { isIphoneX } from 'react-native-iphone-x-helper';
export default class HeadBar extends Component {
  render() {
    return (
      !this.props.isSearchScreen ?
      <View style={[FLEX.RrNwSbCC,styles.headBar,this.props.style]}>
          <View style={[styles.parts,{marginRight:10,alignItems:'flex-end'}]}>{this.props.right}</View>
          <View style={[styles.parts,{alignItems:'center'}]}>{this.props.center}</View>
          <View style={[styles.parts,{marginLeft:10,alignItems:'flex-start'}]}>{this.props.left}</View>
      </View> : 
      <View style={[FLEX.RrNwSbCC,styles.headBar]}>
          {this.props.center}
      </View>
    )
  }
}
const styles = StyleSheet.create({
    headBar: {
        width:'100%',
        paddingBottom:10,
        paddingTop:isIphoneX() ? 45 : 25,
        height: SIZES.headBarHeight,
        backgroundColor : 'white',
        borderBottomWidth: 1,
        borderBottomColor:  COLORS.LighterGrey
    },
    parts : {
        width:3 *  WIN.width / 8 - 40
    }
})
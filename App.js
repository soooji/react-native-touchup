import React from 'react'
import { createBottomTabNavigator, createAppContainer} from 'react-navigation'
import { TouchableOpacity, Text, YellowBox } from 'react-native'
YellowBox.ignoreWarnings([" "]);
import { COLORS, Normalize } from './Base/functions'
import Icon from 'react-native-vector-icons/Feather'

import HomeScreen from './Components/Home/index.js'
import SearchScreen from './Components/Search/index'
import PostScreen from './Components/Post/index';
import BasketScreen from './Components/Basket/index';
import ProfileScreen from './Components/Profile/index';
import { isIphoneX } from 'react-native-iphone-x-helper';

Text.defaultProps = {
  allowFontScaling: false
}
TouchableOpacity.defaultProps = {
  activeOpacity: 1
}

const TabNavigator = createBottomTabNavigator({
  Home: HomeScreen,
  Search: SearchScreen,
  Post: PostScreen,
  Basket: BasketScreen,
  Profile: ProfileScreen
}, {
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state
      let iconName
      switch (routeName) {
        case 'Home':
          iconName = `home`
          break
        case 'Search':
          iconName = `search`
          break
        case 'Post':
          iconName = `plus-circle`
          break
        case 'Basket':
          iconName = `shopping-cart`
          break
        case 'Profile':
          iconName = `user`
          break
        default:
          break
      }
      return <Icon name={iconName} size={Normalize(23)} color={routeName!="Post" ? tintColor : COLORS.MainColor} />
    }
  }),
  tabBarOptions: {
    activeTintColor: 'black',
    inactiveTintColor: COLORS.LightGrey,
    showLabel: false,
    style: {
      borderTopColor: COLORS.LighterGrey,
      borderTopWidth: 1,
      paddingTop: isIphoneX() ? 10 : 0
    },
    showIcon: true
  }
})

export default createAppContainer(TabNavigator)
